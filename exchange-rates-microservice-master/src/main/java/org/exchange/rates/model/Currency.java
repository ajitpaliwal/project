package org.exchange.rates.model;

import lombok.AllArgsConstructor;

public enum Currency {

    EUR,
    GBP,
    HKD,
    USD;
}
