package org.exchange.rates.model.dto;


import lombok.Getter;
import lombok.Setter;

import org.exchange.rates.model.Currency;
import org.springframework.web.bind.annotation.ResponseBody;

@Setter
@Getter
@ResponseBody
public class ExchangeRateOutputResource {

    private Currency base;
    private String date;
    private double rate;
    private Currency symbol;
}
