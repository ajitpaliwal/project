package org.exchange.rates.client;

import org.exchange.rates.model.dto.ExchangeRateInputResource;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@FeignClient(name = "exchange-service", url = "http://data.fixer.io/api")
public interface FixerExchangeRateClient {

    @RequestMapping(value = "/latest?symbols=USD&access_key=262870eb1637f372f2315e31e3276bc7", method = GET)
    ExchangeRateInputResource latest();
}
