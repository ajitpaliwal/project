package org.exchange.rates.converter;

import org.exchange.rates.model.entity.ExchangeRate;
import org.exchange.rates.model.dto.ExchangeRateOutputResource;
import org.exchange.rates.model.dto.ExchangeRateResource;
import org.springframework.core.convert.converter.Converter;

public class ExchangeRateToExchangeRateOutputResourceConverter implements Converter<ExchangeRate, ExchangeRateOutputResource> {


    @Override
    public ExchangeRateOutputResource convert(ExchangeRate rate) {
        if (rate==null)
            return null;
        ExchangeRateOutputResource resource = new ExchangeRateOutputResource();
        resource.setBase(rate.getFromCurrency());
        resource.setSymbol(rate.getToCurrency());
        resource.setDate(rate.getMonth()+"-"+rate.getYear());
        resource.setRate(rate.getRate());
        
        return resource;
    }
}
