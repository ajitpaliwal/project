package org.exchange.rates.service;

import org.exchange.rates.exceptions.ResourceNotFoundException;
import org.exchange.rates.model.entity.ExchangeRate;
import org.exchange.rates.persistence.ExchangeRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

import static java.text.MessageFormat.format;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;

@Service
public class ExchangeRateService {
	@Autowired
    private ExchangeRateRepository exchangeRateRepository;

    
   

    public List<ExchangeRate> getCurrentExchangeRate() {
       LocalDate today = LocalDate.now();
    	int month = today.getMonthValue();
    	int year = today.getYear();
    	List<ExchangeRate> exchangeRates=exchangeRateRepository.findByMonthAndYear(month, year);
        if (CollectionUtils.isEmpty(exchangeRates)) {
            throw new ResourceNotFoundException("There is no exchange rate.");
        }
        return exchangeRates;
    }

    public List<ExchangeRate> getExchangeRates() throws ParseException {
    	
    	
        List<ExchangeRate> exchangeRates = exchangeRateRepository.findAll();
        
        if (exchangeRates == null || exchangeRates.isEmpty()) {
            throw new ResourceNotFoundException(format("No exchange rate found."));
        }
        return exchangeRates;
    }

    public void save(ExchangeRate exchangeRate) {
        if (exchangeRate == null) {
            throw new IllegalArgumentException("Exchange rate can not be null");
        }
        exchangeRateRepository.save(exchangeRate);
    }
}
