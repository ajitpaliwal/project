package org.exchange.rates.persistence;

import org.exchange.rates.model.entity.ExchangeRate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
@Repository
public interface ExchangeRateRepository extends CrudRepository<ExchangeRate, String> {

    //ExchangeRate getFirstByOrderByCreatedDesc();

    List<ExchangeRate> createdBetweenOrderByCreatedAsc(Date from, Date to);
    List<ExchangeRate> getFirstByOrderByCreatedDesc();
    List<ExchangeRate> findByMonthAndYear(int month, int year);
    List<ExchangeRate> createdBetweenOrderByCreatedAsc(String format, String format2);
    
    List<ExchangeRate> findAll();
}
