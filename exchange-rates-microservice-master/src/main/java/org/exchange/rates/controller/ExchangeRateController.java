package org.exchange.rates.controller;

import org.exchange.rates.model.entity.ExchangeRate;
import org.exchange.rates.model.dto.ExchangeRateOutputResource;
import org.exchange.rates.model.dto.ExchangeRateResource;
import org.exchange.rates.model.dto.ExchangeRateSeriesResource;
import org.exchange.rates.service.ExchangeRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.exchange.rates.util.Constants.DATE_FORMAT;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class ExchangeRateController {
	@Autowired
    private ExchangeRateService exchangeRateService;
	@Autowired
    private ConversionService exchangeRateConversionService;

   

   /* @RequestMapping(value = "/exchangeRates/latest", method = RequestMethod.GET)
    public ExchangeRateResource getRate() {
        final ExchangeRate exchangeRate = exchangeRateService.getCurrentExchangeRate();
        return exchangeRateConversionService.convert(exchangeRate, ExchangeRateResource.class);
    }*/
    @RequestMapping(value = "/exchangeRates/latest", method = RequestMethod.GET)
    public List<ExchangeRateResource> getRate() {
    	
        final List<ExchangeRate> exchangeRates = exchangeRateService.getCurrentExchangeRate();
        List<ExchangeRateResource> data= exchangeRates.stream().map(n-> exchangeRateConversionService.convert(n, ExchangeRateResource.class)).collect(Collectors.toList());
        return data;
    }

    @RequestMapping(value = "/exchangeRateSeries", method = RequestMethod.GET)
    public List<ExchangeRateOutputResource> getRateSeries() throws ParseException {
    	List<ExchangeRate> exchangeRates = exchangeRateService.getExchangeRates();
        List<ExchangeRateOutputResource> exchangeRateSeriesResources= exchangeRates.stream().map(n-> exchangeRateConversionService.convert(n, ExchangeRateOutputResource.class)).collect(Collectors.toList());
        
       // ExchangeRateOutputResource exchangeRateSeriesResource = exchangeRateConversionService.convert(exchangeRates, ExchangeRateOutputResource.class);
        
        return exchangeRateSeriesResources;
    }
}
